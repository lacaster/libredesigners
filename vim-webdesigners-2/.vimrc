" -----------------------------------------------------------
" [ Vundle ]


set nocompatible              " be iMproved, necessário
filetype off                  " necessário

" define caminho do runtime para incluir Vundle e inicializá-lo

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" deixe Vundle gerenciar Vundle, necessário
Plugin 'VundleVim/Vundle.vim'

" Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'junegunn/goyo.vim'
Plugin 'ap/vim-css-color'


" Plugins - parte 2
Plugin 'scrooloose/nerdcommenter'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-surround'
Plugin 'elzr/vim-json'
Plugin 'mattn/emmet-vim'
Plugin 'SirVer/ultisnips'
Plugin 'algotech/ultisnips-php'
Plugin 'algotech/ultisnips-javascript'
Plugin 'iloginow/vim-stylus'

" extra
Plugin 'airblade/vim-gitgutter'
Plugin 'editorconfig/editorconfig-vim'

call vundle#end()

filetype plugin indent on


" pega referência da pasta ~/.vim
let g:vimDir = $HOME.'/.vim'

" pega referência dos arquivos:
" - settings.vim
" - mappings.vim
" - plugins.vim
let s:configSettings = g:vimDir.'/config/settings.vim'
let s:configMappings = g:vimDir.'/config/mappings.vim'
let s:configPlugins = g:vimDir.'/config/plugins.vim'

" Carrega as configurações no vim de settings, mappings e plugins
exec ":source ".s:configSettings
exec ":source ".s:configMappings
exec ":source ".s:configPlugins
