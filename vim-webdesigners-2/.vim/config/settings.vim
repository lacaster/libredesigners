" ----------------------------------------------------------
" [Setting]

if ! has("gui_running") " se estiver rodando gvim 
    set t_Co=256        " habilita 256 cores antes
endif                   " de definir o esquema de cores 

" esquema de cores
colorscheme monokai-phoenix

" font e tamanho
set guifont=IBM\ Plex\ Mono\ Medium\ 10

" quebra final de linha
set wrap

" mostra número na linha
set number

" realça sintaxe
syntax on


" identação
set smartindent " adiciona um nível extra de identação dependendo do contexto
set autoindent " copia a identação da linha anterior
set copyindent " preserva identacão
set shiftwidth=4 " identação possui 4 espaços
set shiftround " habilita identacao com as teclas << e >> no modo normal
set backspace=indent,eol,start " backspace passa a funcionar como outros programas
set smarttab " usa shiftwidth ao invés da pausa do tab
set expandtab " Troca 4 espaços por tab

" busca
set showmatch " marca visual no resultado da busca no texto
set hlsearch " destaca todas as resultados da busca
set incsearch " destaca o texto ao realizar a busca

" copy/paste
set paste " cola texto com mesma formatação que foi copiado
set clipboard=unnamedplus " usa clipboard do sistema

" alinhamento manual
set foldmethod=manual 

" mouse
set mouse=a "mouse ativo em todos os modos do vim

set guioptions-=m  "remove barra de menu
set guioptions-=T  "remove barra de ferramentas
set guioptions-=L  "remove barra de rolagem

set wildmenu       "completar na linha de comando

set linespace=11 " espaçamento entre as linhas

set history=1000 " quantidade de comandos histórico
set showcmd " mostra comandos na barra de status ao ser digitado
set noswapfile " desativa swap file
"set nonu rnu "número relativo de linha

set enc=utf-8 "codificação utf-8
set fileencoding=utf-8 "codificação do arquivo utf-8

set hidden "esconde buffers ao invés de fechá-los

runtime macros/matchit.vim


" (Folding)
set foldmethod=manual
set nofoldenable

set path+=**

" disable insert (paste) mode
set nopaste
