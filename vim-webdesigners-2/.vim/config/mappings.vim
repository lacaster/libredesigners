" -----------------------------------------------------------
" [Mapeamento]

" leader 
let g:mapleader = "," 

" Abre MYVIMRC em um split
nnoremap <leader>ev :split $MYVIMRC<cr>
" Abre settings.vim em um split
nnoremap <leader>evs :split ~/.vim/config/settings.vim<cr>
" Abre mappings.vim em um split
nnoremap <leader>evm :split ~/.vim/config/mappings.vim<cr>
" Abre plugins.vim em um split
nnoremap <leader>evp :split ~/.vim/config/plugins.vim<cr>

" Source MYVIMRC
nnoremap <leader>sv :source $MYVIMRC<cr>

" move the current line below 
nnoremap <leader>- ddp 

" move the current line above 
nnoremap <leader>_ ddkP

" switch tab
nnoremap <S-right> :tabn<CR>
nnoremap <S-left> :tabp<CR>


" insert mode uppercase the current word
"  <esc> : go to normal mode
"  v 	 : visual mode
"  iw 	 : select the current word
"  U 	 : uppercase selection
"  i 	 : back to insert mode
inoremap <c-u> <esc>viwUi


" remove last search highlight
nnoremap <C-l> :nohlsearch<CR><C-l>


" Wrap a word in double quotes
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel

" Wrap a word in single quotes
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel


" select inside parents
onoremap in( :<c-u>normal! f(vi(<cr>

" select inside braces
onoremap in{ :<c-u>normal! f{vi{<cr>

" select inside brackets
onoremap in[ :<c-u>normal! f[vi[<cr>

" Leave insert mode (like <esc>) and disable <esc>
inoremap jk <esc>
" inoremap <special> <esc> <nop>
inoremap <esc>^[ <esc>^[

" Disable arrow keys

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>

inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

nnoremap <Leader>b :ls<CR>:b<Space>

"let NERDTreeChDirMode=2
nnoremap <leader>n :NERDTreeToggle <CR>
nnoremap <C-Tab> :b# <CR>
nnoremap o o<Esc>
nnoremap O O<Esc>

nnoremap <leader>o :only <CR>

inoremap <C-Space> <C-n>

inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>

" Limpa ^M
nnoremap <leader>l :%s/\r//g<cr>
