

" [Plugins]

" (nerdtree)

let g:NERDTreeChDirMode=2
nnoremap <leader>n :NERDTreeToggle <CR>

" (goyo)

nnoremap <leader>g :Goyo <CR>




" --------------------------------------------------
" [Plugins]
"
" ( NERDTree )
" on vim enter opens nerd tree

function! OpenNerdTree()
    let s:exclude = ['COMMIT_EDITMSG', 'MERGE_MSG']
    if index(s:exclude, expand('%:t')) < 0
        NERDTreeFind
        exec "normal! \<c-w>\<c-w>"
    endif
endfunction
autocmd VimEnter * call OpenNerdTree()


" redimensiona janela do nerd tree
let g:NERDTreeWinSize = 35

" mostra arquivos ocultos
let g:NERDTreeShowHidden=1

" ignora arquivos .swp
let g:NERDTreeIgnore=['\.swp$', '\~$']
nnoremap <c-n> :NERDTreeToggle<cr>

" ajuda ao sair quando não existem buffers abertos além do NerdTree
function! CheckLeftBuffers()
  if tabpagenr('$') == 1
    let i = 1
    while i <= winnr('$')
      if getbufvar(winbufnr(i), '&buftype') == 'help' ||
          \ getbufvar(winbufnr(i), '&buftype') == 'quickfix' ||
          \ exists('t:NERDTreeBufName') &&
          \   bufname(winbufnr(i)) == t:NERDTreeBufName ||
          \ bufname(winbufnr(i)) == '__Tag_List__'
        let i += 1
      else
        break
      endif
    endwhile
    if i == winnr('$') + 1
      qall
    endif
    unlet i
  endif
endfunction
autocmd BufEnter * call CheckLeftBuffers()





" ( multiple cursor ]

let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<C-m>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<esc>'


" (Goyo)
nnoremap <leader>g :Goyo <CR>


" (Nerd commenter)
noremap <c-_> :call NERDComment(0, "Toggle")<cr>

" (Ctrl+p)
nnoremap <C-M-p> :CtrlPBuffer <CR>

