" -----------------------------------------------------------
" [ Vundle ]


set nocompatible              " be iMproved, necessário
filetype off                  " necessário

" define caminho do runtime para incluir Vundle e inicializá-lo

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" deixe Vundle gerenciar Vundle, necessário
Plugin 'VundleVim/Vundle.vim'

" Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'junegunn/goyo.vim'
Plugin 'ap/vim-css-color'


call vundle#end()
filetype plugin indent on



" -----------------------------------------------------------
" [Mapeamento]

" leader 
let g:mapleader = "," 

" Abre MYVIMRC em um split
nnoremap <leader>ev :split $MYVIMRC<cr>

" Source MYVIMRC
nnoremap <leader>sv :source $MYVIMRC<cr>

" muda comportamento da tecla o e O
" adiciona uma linha abaixo
nnoremap o o<Esc> 

" adiciona uma linha acima
nnoremap O O<Esc> 



" alterna buffers mais recentes com ctrl+tab
nnoremap <C-Tab> :b# <CR>



" troca de abas com shift + seta esquerda e shift + seta direita

" navega pra aba a direita
nnoremap <S-right> :tabn<CR>

 " navega pra aba a esquerda
nnoremap <S-left> :tabp<CR>


" remove marcação da ultima busca com ctrl + l
nnoremap <C-l> :nohlsearch<CR><C-l>

" Envelopa uma palavra em aspas duplas com ," no modo normal
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel

" Envelopa uma palavra em aspas simples com ,' no modo normal
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel

" Seleciona dentro dos parênteses
onoremap in( :<c-u>normal! f(vi(<cr>

" Selecina dentro dos chaves
onoremap in{ :<c-u>normal! f{vi{<cr>

" Seleciona dentro das colchetes
onoremap in[ :<c-u>normal! f[vi[<cr>




" -----------------------------------------------------------
" [Setting]

if ! has("gui_running") " se estiver rodando gvim 
    set t_Co=256        " habilita 256 cores antes
endif                   " de definir o esquema de cores 

" esquema de cores
colorscheme monokai-phoenix

" font e tamanho
set guifont=IBM\ Plex\ Mono\ Medium\ 11

" quebra final de linha
set wrap

" mostra número na linha
set number

" realça sintaxe
syntax on


" identação
set smartindent " adiciona um nível extra de identação dependendo do contexto
set autoindent " copia a identação da linha anterior
set copyindent " preserva identacão
set shiftwidth=4 " identação possui 4 espaços
set shiftround " habilita identacao com as teclas << e >> no modo normal
set backspace=indent,eol,start " backspace passa a funcionar como outros programas
set smarttab " usa shiftwidth ao invés da pausa do tab
set expandtab " Troca 4 espaços por tab

" busca
set showmatch " marca visual no resultado da busca no texto
set hlsearch " destaca todas as resultados da busca
set incsearch " destaca o texto ao realizar a busca

" copy/paste
set paste " cola texto com mesma formatação que foi copiado
set clipboard=unnamedplus " usa clipboard do sistema

" alinhamento manual
set foldmethod=manual 

" mouse
set mouse=a "mouse ativo em todos os modos do vim

set guioptions-=m  "remove barra de menu
set guioptions-=T  "remove barra de ferramentas
set guioptions-=L  "remove barra de rolagem

set wildmenu       "completar na linha de comando

set linespace=2 " espaçamento entre as linhas

set history=1000 " quantidade de comandos histórico
set showcmd " mostra comandos na barra de status ao ser digitado
set noswapfile " desativa swap file
set nonu rnu "número relativo de linha

set enc=utf-8 "codificação utf-8
set fileencoding=utf-8 "codificação do arquivo utf-8

set hidden "esconde buffers ao invés de fechá-los





" [Plugins]

" (nerdtree)

let g:NERDTreeChDirMode=2
nnoremap <leader>n :NERDTreeToggle <CR>

" (goyo)

nnoremap <leader>g :Goyo <CR>
